---
puppeteer:
  landscape: true
  format: "A4"
  timeout: 3000 # <= Special config, which means waitFor 3000 ms
---

# Название

- список 
- список
- list

# Еще название 

## Важный раздел

> Важная цитата

``` python
x = 1
if x == 1:
    # indented four spaces
    print("x is 1.")
```

![Картиночка](/img/docs1.jpeg)



