---
html:
  embed_local_images: true
  embed_svg: true
  offline: false
  toc: undefined
print_background: false
---