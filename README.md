# webinar

Конспект вебинара тут: [Материалы вебинара](/post-webinar-materials.md). 

На вебинаре мы рассмотрели следующие расширения: 

- [Preview](https://marketplace.visualstudio.com/items?itemName=searKing.preview-vscode)
- [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one#keyboard-shortcuts-1)
- [Remark](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-remark)
- [Rewrap](https://marketplace.visualstudio.com/items?itemName=stkb.rewrap)
- [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
- [Markdown Shortcuts](https://marketplace.visualstudio.com/items?itemName=mdickin.markdown-shortcuts)
- [RestructuredText](https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext)
- [HTTP/s and relative link checker](https://marketplace.visualstudio.com/items?itemName=blackmist.LinkCheckMD)
- [CodeSpell](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
- [Polacode](https://marketplace.visualstudio.com/items?itemName=pnp.polacode)
- [Word Count](https://marketplace.visualstudio.com/items?itemName=ms-vscode.wordcounts). 


## Еще полезные ссылки:

1. [Docs Authoring Pack от Microsoft](https://marketplace.visualstudio.com/items?itemName=docsmsft.docs-authoring-pack)
2. [Пост](https://t.me/technical_writing/340) в канале [Technical Writing 101](https://t.me/technical_writing) про то, как настроить себе VS Code во вкладке хрома при помощи code-server
3. [Github Pull Requests](https://marketplace.visualstudio.com/items?itemName=GitHub.vscode-pull-request-github&WT.mc_id=vscodecandothat-dotcom-team)
4. [Аналог Git Patch в VS Code](https://marketplace.visualstudio.com/items?itemName=paragdiwan.gitpatch).

